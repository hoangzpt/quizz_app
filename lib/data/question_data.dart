import 'package:quizz_app/models/question.dart';

List<Question> questions = [
  Question(
    text: 'What are the main building blocks of Flutter UIs?',
    answer: [
      'Widgets',
      'Components',
      'Blocks',
      'Functions',
    ],
  ),
  Question(
    text: 'Vietnam Capital',
    answer: [
      'Ha Noi',
      'Hai Phong',
      'Da Nang',
      'Vinh Phuc',
    ],
  ),
];
