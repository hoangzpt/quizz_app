import 'package:flutter/material.dart';
import 'package:quizz_app/questions_summary/summary_item.dart';

class QuestionsSummary extends StatelessWidget {
  final List<Map<String, Object>> summaryData;

  const QuestionsSummary({Key? key, required this.summaryData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      child: SingleChildScrollView(
        child: Column(
          children: summaryData.map((e) {
            return SummaryItem(itemData: e);
          }).toList(),
        ),
      ),
    );
  }
}
