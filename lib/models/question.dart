class Question {
  final String text;
  final List<String> answer;

  Question({required this.text, required this.answer});

  List<String> get shuffledAnswers {
    final shuffledList = List.of(answer);
    shuffledList.shuffle();
    return shuffledList;
  }
}
